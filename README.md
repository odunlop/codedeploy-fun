# CodeDeploy
The following is how I got CodeDeploy working for a js node project using GitLab and MongoDB. Copies of the `gitlab-ci.yml` and `appspec.yml` files are included in this project.

![A diagram of how it all works](./img/diagram.png)

## GitLab CI/CD Pipeline
Firstly, I set up a Pipeline on GitLab which would 
1) **Build**: Zip the project 
2) **Test**: Run all tests 
3) **Deploy**: Deploy the .zip file of our project to an S3 Bucket (later in the project, I add to this pipeline to include deploying to CodeDeploy)

### Build
The following zips the project and stores the `proj-zip` folder for use later in the pipeline.

```
zip project:
   stage: build
   image: python:latest
   script:
      - apt-get update -y
      - apt-get install zip
      - mkdir proj-zip
      - zip -r proj-zip/acebook.zip .
      - cd proj-zip
      - ls
   artifacts:
      paths:
         - proj-zip
```

### Test
First we need to set up a test database in MongoDB for our test stage using GitLab's `services` functionality:

```
services: 
   - name: mongo:4.2

variables:
   MONGODB_URL: 'mongodb://mongo:27017/acebook_test'
```

Then we can proceed with testing:

```
test_unit:
  stage: test
  script:
    - npm install
    - npm start &
    - npm run lint
    - npm run test:unit
  only:
      changes:
         - "*.js"

test_integration:
   stage: test
   image: cypress/base:12.14.1
   script:
     - npm install
     - npm start &
     - npm run start:ci &
     - npm run test:integration
   only:
      changes:
         - "*.js"
         - "*.hbs"
```

For the above...
* `npm install` -> installs the project's dependencies 
* `npm start &` -> starts the server and the ampsand symbol is vital for making sure our project doesn't get stuck here

### Deploy
Now to deploy! For this step to work we need an S3 Bucket already set up in AWS, and the following variables defined in olur GitLab CI/CD settings:
* AWS_DEFAULT_REGION
* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY_ID
* S3_BUCKET

```
deploy to s3:
   stage: deploy
   image:
     name: amazon/aws-cli
     entrypoint: [""]
   script:
      - aws --version
      - aws s3 rm s3://$S3_BUCKET --recursive
      - cd proj-zip
      - aws s3 cp . s3://$S3_BUCKET --recursive
```
In the above, we empty the S3 Bucket just in case of any weird duplications and then copy in our zip file (which is saved in our proj-zip folder)


## AWS EC2 Instance
Before we can add CodeDeploy to our pipeline we need to set up an EC2 instance which will host the project.
Our EC2 instance will need a custom **IAM** role to work properly, created via these steps in AWS:
1) IAM > Roles > Create Role > EC2
2) Add `AmazonEC2RoleforAWSCodeDeploy`
3) Give descriptive name (e.g. "EC2InstanceRole")

Now we launch a new EC2 Instance, making sure to select the role we just made (e.g. "EC2InstanceRole") for the `IAM Role` option and we will paste the following code into the `User Data` section to make sure our CodeDeploy Agent is alive:
```
#!/bin/bash
yum -y update
yum install -y ruby
yum install -y aws-cli
aws s3 cp s3://aws-codedeploy-eu-west-2/latest/install . --region eu-west-2
chmod +x ./install
./install auto
```
Also good to add tags (e.g. "Name" -> "orla-acebook") so we can identify it later in CodeDeploy. 

We also need to go into the instance and change the security settings to make sure the port we need to exposed. For this project it was port 3000.
1. EC2 > Instances > Our Instance > Security
2. Click the link under "Security Groups"
3. Click "Edit inbound rules" > "Add rule"
4. Under `Port Range` enter "3000"

*If we want to ssh into our EC2 instance later, we will need to create a key-pair and download the `.pem` file. Remembering, that if we wanna ssh using this file, we need to be in the same folder as it in our terminal*

## CodeDeploy
More IAM permissions! This time for CodeDeploy:
1. IAM > Roles > Create Role > CodeDeploy
2. Add policy `AWSCodeDeployRole`
3. Again, meaningful name (e.g. "MyCodeDeployRole")

Setting up CodeDeploy comes in two parts: 1) Creating the application 2) Creating the deployment group
1. CodeDeploy > Deploy > Applications > Create Application
    * Compute platform = "EC2 / On-premises"
2. Applications > Click on application name > Create deployment group
    * Service Role = "MyCodeDeployRole"
    * Deployment Type = "In-Place"
    * Environment configuration = "Amazon EC2 Instance"
    * Un-tick "Load Balancer" 

To make our deployment work, we need an `appspec.yml` file in the root of our project. An example of the one I used for **acebook** can be found in this repo, alongside the `scripts` folder containing my `.sh` files. 

The `appspec.yml` file contained the following instructions:
1. Install the dependencies for this project using `nvm install` and `npm install`
2. Set up the MongoDB database within the EC2 instance
3. Start the server using `npm run start`

## Complete AUTOMATION!!
We can now add a line to our `.gitlab-ci.yml` file which automates our project even futher - so after we've successfully updated our S3 Bucket with our .zip file, the GitLab Pipeline then also triggers our CodeDeploy deployment so our EC2 instance gets updated. All done in one line of code!

```
aws deploy create-deployment --application-name $AWS_APPLICATION_NAME --s3-location bucket=$S3_BUCKET,key=acebook.zip,bundleType=zip --deployment-group-name $AWS_DEPLOYMENT_GROUP    
```
(Can find the full pipeline file in this repo as well.)

This addition requires us to add the following variables to our CI/CD settings in GitLab:
* AWS_APPLICATION_NAME
* AWS_DEPLOYMENT_GROUP 
